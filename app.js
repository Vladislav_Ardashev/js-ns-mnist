﻿const express = require('express');
const fs = require('fs');
const http = require('http');
const app = express();
const server = http.createServer(app);
const io = require('socket.io').listen(server);
const nn = require('./perceptron.js');
const ds = require('./dataset')();
const Jimp = require("jimp");

app.use(express.static(__dirname + '/public'));

// обучение
const train = function() {
    (async () => {

        await ds.loadTrainingSet();

        const net = nn({
            log: 1,
            layers: [ 20 ]
        });

        net.train(ds.trainingSet, "nets/net_digits_" + Date.now() + ".txt");

    })();
};

// обработка событий от клиента
io.on('connection', function(socket){

    // считаем данные обученной сети
    const net = nn().fromJSON(fs.readFileSync('nets/best_dig.txt'));

    // обработка запроса на распознавание
    socket.on('predict', async function(input) {

        // сохраним изображение во временный файл
        let data = input.replace(/^data:image\/\w+;base64,/, "");
        let buf = new Buffer(data, 'base64');
        let filename = 'input/input' + Date.now() + '.png';
        await fs.writeFileSync(filename, buf);

        Jimp.read(filename, async function (err, image) {

            // ресайз изображения до 28х28
            image.resize(28, 28)
                .quality(100);

            // преобразование в ч/б
            var nnInput = [];
            for (var i = 0; i < image.bitmap.height * image.bitmap.width * 4; i += 4) {
                var red = 255 - image.bitmap.data[i];
                var green = 255 - image.bitmap.data[i + 1];
                var blue = 255 - image.bitmap.data[i + 2];
                var gray = 0.299 * red + 0.587 * green + 0.114 * blue;
                nnInput.push(gray > 150 ? 1 : 0);
            }

            // распознавание
            let output = net.send(nnInput);

            // вывод информации
            for (let i = 0; i < 28; i++) {
                let line = '';
                for (let j = 0; j < 28; j++) {
                    line += nnInput[i * 28 + j];
                }
                console.log(line);
            }

            // вычислим цифру на основе выхода сети
            let predicted = output.indexOf(Math.max(...output));

            // вывод информации
            console.log(output[predicted]);

            // отправка распознанной цифры клиенту
            io.emit('predicted', predicted);

            await fs.unlinkSync(filename);

        });

    });


});


app.get('/', function(req, res) {
	res.render('index.html');
});

const port = 3000;

server.listen(port, function() {
    console.log("Listening on " + port);
});