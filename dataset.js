let fs = require("fs");
let getPixels = require("get-pixels");

module.exports = function () {
    return new Dataset()
};

/**
 * Класс для работы с данными
 */
class Dataset {

    constructor() {
        this.filenames = [];
        this.trainingSet = [];
    }

    /**
     * Загружает обучающую выборку из MNIST Dataset (рукописные цифры)
     */
    loadTrainingSetMnist() {
        let self = this; // текущий контекст
        let content = fs.readFileSync("dataset/mnist_train.csv"); // данные из файла
        let lines = content.toString().split('\n').shuffle().slice(0, 5000); // строчки

        // пройдемся по строчкам
        lines.forEach(function(line) {
            // разделим строчку на массив цифр
            line = line.split(',').map((s) => parseInt(s));
            // массив представляющий одну цифру
            let digit = line.slice(1, line.length);
            // приведем изображение к ч/б формату
            digit = digit.map(function(pixel) {
                return pixel > 230 ? 1 : 0;
            });
            // преобразуем ответ к массиву
            // 0 -> [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            // 1 -> [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
            // ...
            // 9 -> [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
            let label = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            label[line.slice(0, 1)] = 1;

            // добавим полученные данные в обучающую выборку
            self.addTrainingExample(digit, label)
        });
    }

    /**
     * Получает массив пикселей изображения
     * @param filename
     * @returns {Promise}
     */
    getImage(filename) {
        return new Promise(function(resolve, reject) {
            getPixels(filename, function(err, pixels) {
                if(err) {
                    reject("Неверный путь");
                }
                resolve(pixels);
            });
        });
    }

    /**
     * Загружает обучающую выборку
     * @returns {Promise.<void>}
     */
    async loadTrainingSet() {
        const self = this,
              datasetDir = "dataset/digits_28";

        //считаем названия файлов
        fs.readdirSync(datasetDir).forEach(file => {
            this.filenames.push(datasetDir  + '/' + file);
        });
        // перемешаем
        this.filenames = this.filenames.shuffle();

        let rawFiles = [];

        // пройдемся по всем изображениям, и загрузим информацию о пикселях
        for(let i in this.filenames) {
            if (this.filenames.hasOwnProperty(i)) {
                const filename = this.filenames[i],
                      label = filename.split('/').slice(-1)[0].split('.')[0].split('_').slice(-1)[0];
                rawFiles.push({label: label, pixels: await this.getImage(this.filenames[i])});
            }
        }

        // конвертируем изображения в ч/б
        // преобразуем ответы
        // и добавим данные в обучающую выборку
        rawFiles.forEach(function(rawFile) {
            const pixels = rawFile.pixels.data;
            let bwPixels = [];
            for (let i = 0; i < pixels.length; i+=4) {
                const red = 255 - rawFile.pixels.data[i];
                const green = 255 - rawFile.pixels.data[i + 1];
                const blue = 255 - rawFile.pixels.data[i + 2];
                const grey = 0.299 * red + 0.587 * green + 0.114 * blue;
                bwPixels.push(grey > 150 ? 1 : 0);
            }
            // преобразуем ответ к массиву
            // 0 -> [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            // 1 -> [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
            // ...
            // 9 -> [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
            let label = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            label[rawFile.label] = 1;
            // добавим в выборку
            self.addTrainingExample(bwPixels, label)
        });
    }

    /**
     * Добавление данных в обучающую выборку
     * @param input
     * @param output
     */
    addTrainingExample(input, output) {
        this.trainingSet.push({input: input, output: output});
    }

}

/**
 * Перемешивает массив
 * @returns {Array}
 */
Array.prototype.shuffle = function () {
    let a = this;
    for (let i = a.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
};