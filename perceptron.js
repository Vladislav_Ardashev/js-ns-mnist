const util = require('util');
const fs = require('fs');

const DEFAULT_OPTIONS = {
    // конфигурация скрытых слоев, [ 2, 1 ] -> 2 скрытых слоя, 2 нейрона в первом и 1 во втором
    layers: [ 3 ],
    // максимальное число итераций обучения
    epochs: 20000,
    // минимально допустимый порог ошибки
    errorThreshold: 0.005,
    // коэф-т скорости обучения
    learningRate: 0.4,
    // импульс, корректирующий коэф-т
    momentum: 0.5,
    // частота логирования, 1 -> каждую итерацию, 10 -> каждые 10, и т.д.
    log: 0
};

module.exports = function (options) {
    return new Perceptron(options)
};

/**
 * Класс нейронной сети
 */
class Perceptron {

    /**
     * @param options параметры сети
     * @constructor
     */
    constructor(options) {
        // установка параметров
        this.setOptions(options);

        // Количество слоев
        // 1 входной + 1 выходной + скрытые
        this.numberOfLayers = 2 + this.options.layers.length;

        // инициализация весов в скрытых слоях
        this.setupHiddenLayers()
    }

    /**
     * Получение случайного значения
     * @returns {number}
     */
    static getRandom() {
        return Math.random() * 0.2 - 0.3
    }

    /**
     * Обучение сети на тренировочных данных
     * @param  {Array|Object} data данные для обучения -> [ { input: [], output: [] }, ... ]
     * @param  {string} file Файл для сохранения сети
     */
    train(data, file) {
        if (!Array.isArray(data)) {
            data = [ data ]
        }

        const self = this, // текущий контекст
              epochs = this.options.epochs; // количество итераций

        let epoch = 0, // текущая итерация
            mse = 1; // среднеквадратическая ошибка, Mean Squared Error

        // инициализация весов выходного слоя
        this.setupOutputLayer(data[0].output);
        this.changes = [];

        // пока не достигнуто максимально число итераций,
        // и ошибка больше минимально допустимой
        while (epoch++ < epochs && mse > this.options.errorThreshold) {

            // цикл по всем обучающим примерам
            data.forEach(function (trainingExample) {
                const input = trainingExample.input, // данные обучающего примера
                      expected = trainingExample.output; // ожидаемый ответ

                // инициализация весов входного слоя
                if (!self.inputLayerSetup) {
                    self.setupInputLayer(input);
                    self.inputLayerSetup = true
                }

                // вычислим ответ для текущего обучающего примера.
                // в ходе распространения (propagate) сигналов от входного слоя к выходному
                // будут вычислины выходные сигналы всех нейронов - это необходимо для обучения сети
                self.send(input);

                // запустим алгоритм обучения.

                // Метод обратного распространения ошибки:
                // распространение сигналов ошибки от выходов сети к её входам,
                // в направлении, обратном прямому распространению сигналов
                // в обычном режиме работы.
                self.backPropagate(expected);
            });

            // вычислим mse - если она достаточно низкая,
            // можно прекратить обучение

            let sumSquaredErrors = 0; // суммарная квадратическая ошибка

            // пройдемся по всем обучающим примерам
            data.forEach(function (trainingExample) {
                // вычислим ответ для обучающего примера
                const output = self.send(trainingExample.input);

                // посчитаем ошибку для полученного ответа
                trainingExample.output.forEach(function (expectedOut, index) {
                    const rawError = expectedOut - (output[index] || 0);
                    sumSquaredErrors += Math.pow(rawError, 2);
                })
            });

            // вычислим среднеквадратическую ошибку
            mse = sumSquaredErrors / data.length;

            // выведем текущее сосотояние обучения
            if (self.options.log && epoch%self.options.log === 0) {
                console.log('Итерация %s. MSE: %s.', epoch, mse)
            }

            // сохраним состояние сети
            fs.writeFileSync(file, this.toJSON());
        }
    }

    /**
     * Вычисление значение в выходном слое.
     * Распространение сигналов от входного слоя к выходному
     * @param  {Array|Object} input данные
     */
    send(input) {
        const self = this; // текущий контекст

        this.outputs = []; // выходные сигналы нейронов
        this.outputs[0] = input; // выходные сигналы первого слоя - это входные данные

        // пройдемся по всем слоям
        for (let layer = 1; layer < this.numberOfLayers; layer++) {
            const numberOfNeurons = this.weights[layer].length; // количество нейронов в слое

            this.outputs[layer] = [];

            // запомним значения выходов предыдущего слоя
            const prevLayerOutput = this.outputs[layer - 1];

            // пройдемся по всем нейронам в слое
            for (let n = 0; n < numberOfNeurons; n++) {

                // вычислим выход текущего нейрона
                let neuronOut;

                // инициализируем его смещением
                neuronOut = this.biases[layer][n];

                // и добавим взвешенную сумму выходов предыдущего слоя, умноженного на вес текущего слоя
                prevLayerOutput.forEach(function (prevOut, p) {
                    neuronOut += prevOut * self.weights[layer][n][p]
                });

                // вычислим выход нейрона с помощью функции активации
                neuronOut = Perceptron.activate(neuronOut);

                // добавим его в массив выходных сигналов текущего слоя
                this.outputs[layer][n] = neuronOut;

            }
        }

        // возвращаем значения выходного слоя
        return this.outputs[this.outputs.length - 1]
    }

    /**
     * Метод обратного распространенения ошибок.
     * Использует ожидаемые (expected) значения
     * для определения значений ошибок и обновления весов (weights) / смещений (biases)
     * @param expected
     */
    backPropagate(expected) {
        let self              = this, // текущий контекст
            n                 = 0, // счетчик
            p                 = 0, // счетчик
            d                 = 0, // счетчик
            outputLayerIndex  = this.outputs.length - 1, // индекс выходного слоя
            outputLayer       = this.outputs[outputLayerIndex], // значения в выходном слое
            prevLayerOutput   = this.outputs[outputLayerIndex - 1], // выходные значения предпоследнего слоя
            numberOfOutputs   = outputLayer.length; // количество выходов сети

        // коэффициенты корректировки весов связей.
        // Используются для распространиения ошибок от выходного слоя к входному
        this.errorSignals = [];
        this.errorSignals[outputLayerIndex] = [];

        // инициализируем массив изменения весов
        if (!this.changes[outputLayerIndex])
            this.initializeChanges();

        // обновим веса для выходного слоя

        // пройдемся по всем выходам
        for (n = 0; n < numberOfOutputs; n++) {

            const expectedOut = expected[n]; // ожидаемый выход
            let predictedOut = outputLayer[n]; // полученный выход
            const neuronError = expectedOut - predictedOut; // величина ошибка

            // вычислим коэффициент корректировки
            let errorSignal = neuronError * predictedOut * (1 - predictedOut);
            self.errorSignals[outputLayerIndex][n] = errorSignal;

            // обновим веса связей в соотвествии с коэффициентом корректировки
            for (p = 0; p < prevLayerOutput.length; p++) {

                let change = self.changes[outputLayerIndex][n][p];
                let weightDelta = self.options.learningRate * errorSignal * prevLayerOutput[p];

                change = weightDelta + (self.options.momentum * change);

                this.weights[outputLayerIndex][n][p] += change;
                this.changes[outputLayerIndex][n][p] = change
            }

            // корректировка смещения
            const biasDelta = self.options.learningRate * errorSignal;
            this.biases[outputLayerIndex][n] += biasDelta
        }

        // обновим веса для остальных слоев

        const lastHiddenLayerNum = outputLayerIndex - 1; // индекс последнего скрытого слоя

        // пройдемся по остальным слоям
        // от последнего к первому
        for (let layer = lastHiddenLayerNum; layer > 0; layer--) {

            let prevLayerOutput = this.outputs[layer - 1]; // выходы предыдущего слоя
            const nextLayerSize = this.outputs[layer + 1].length; // количество нейронов в следующем слое

            this.errorSignals[layer] = [];

            // определим ошибку каждого нейрона в текущем слое
            for (n = 0; n < this.outputs[layer].length; n++) {
                let neuronOut = this.outputs[layer][n]; // выход текущего нейрона

                // взвешенная сумма ошибок следующего слоя
                let nextLayerErrorSum = 0;

                // вычислим ошибки для каждой связи текущего нейрона
                for (d = 0; d < nextLayerSize; d++) {
                    nextLayerErrorSum += this.errorSignals[layer + 1][d] * (self.weights[layer + 1][d][n] || 0)
                }

                // вычислим коэффициент корректировки для текущего нейрона
                let errorSignal = nextLayerErrorSum * neuronOut * (1 - neuronOut);
                this.errorSignals[layer][n] = errorSignal;

                // обновим веса связей в соотвествии с коэффициентом корректировки
                for (p = 0; p < prevLayerOutput.length; p++) {

                    let change = this.changes[layer][n][p];
                    let weightDelta = this.options.learningRate * errorSignal * prevLayerOutput[p];

                    change = weightDelta + (this.options.momentum * change);

                    this.weights[layer][n][p] += change;
                    this.changes[layer][n][p] = change;
                }

                // обновим смещение
                this.biases[layer][n] += this.options.learningRate * errorSignal
            }
        }
    }

    /**
     * Инициализация массива изменений весов,
     * который используется для отслеживания коэффициента импульса (momentum)
     * на этапе обучения. Изначально содержит нули
     */
    initializeChanges() {
        const self = this;

        this.changes = [];
        this.changes.push([]);

        this.outputs.slice(1).forEach(function (layer, index) {
            self.changes.push(layer.map(function () {
                const out = [],
                      prevSize = self.outputs[index].length;

                for (let i = 0; i < prevSize; i++) {
                    out.push(0);
                }

                return out;
            }))
        })
    }

    /**
     * Инициализация весов и смещений для первого скрытого слоя
     * на основе входных данных
     * @param input входные данные
     */
    setupInputLayer(input) {
        const index = 1, // номер слоя
              numberOfInputs = input.length, // количество входных значений
              numberOfNeurons = this.options.layers[0]; // колечество нейронов в первом сс=крытом слое

        this.biases[index] = [];
        this.weights[index] = [];

        // проедемся по всем нейронам
        for (let n = 0; n < numberOfNeurons; n++) {
            // инициализируем смещение для первого скрытого слоя
            this.biases[index][n] = Perceptron.getRandom();
            this.weights[index][n] = [];

            // пройдемся по всем входам
            for (let i = 0; i < numberOfInputs; i++) {
                // и иницализируем веса всех входных связей (синапсов)
                // для текущего нейрона
                this.weights[index][n][i] = Perceptron.getRandom();
            }
        }
    }

    /**
     * Инициализация весов и смещений для выходного слоя
     * @param expected выходные значения
     */
    setupOutputLayer(expected) {
        const index = this.numberOfLayers - 1, // номер последнего слоя
              numberOfInputs = this.options.layers[this.options.layers.length - 1], // количество неронов на последнем скрытом слое
              numberOfOutputs = expected.length; // количество выходов в последнем слое

        this.biases[index] = [];
        this.weights[index] = [];

        // пройдемся по всем выходам
        for (let n = 0; n < numberOfOutputs; n++) {
            // инициализируем смещение
            this.biases[index][n] = Perceptron.getRandom();
            this.weights[index][n] = [];

            // пройдемся по всем входам
            for (let i = 0; i < numberOfInputs; i++) {
                // и иницализируем веса всех входных связей (синапсов)
                // для текущего выхода
                this.weights[index][n][i] = Perceptron.getRandom();
            }
        }
    }

    /**
     * Иницализация весов в скрытых слоях
     */
    setupHiddenLayers() {
        this.biases = []; // веса bias-нейронов (нейроны смещения)
        this.weights = []; // веса нейронов

        const layers = this.options.layers; // конфигурация скрытых нейронов

        // инициализируем каждый скрытый слой, кроме первого.
        // он будет использоваться в качестве входного слоя
        for (let layer = 0; layer < layers.length; layer++) {
            const numberOfNeurons = layers[layer]; // количество нейронов в слое
            let n = 0; // счетчик

            this.biases[layer + 1] = [];
            this.weights[layer + 1] = [];

            // инициализируем смещения для текущего слоя
            for (n = 0; n < numberOfNeurons; n++) {
                this.biases[layer + 1][n] = Perceptron.getRandom();
            }

            // не устанавливаем первый скрытый слой до тех пор, пока не начнем обучение.
            // таким образом мы можем адаптировать сеть к данным обучения.
            if (layer > 0) {
                const numberOfInputs = layers[layer - 1];

                for (n = 0; n < numberOfNeurons; n++) {
                    this.weights[layer + 1][n] = [];

                    for (let i = 0; i < numberOfInputs; i++) {
                        this.weights[layer + 1][n][i] = Perceptron.getRandom();
                    }
                }
            }
        }
    }

    /**
     * Вывод в консоль текущих весов сети
     */
    print() {
        console.log('weights:', util.inspect(this.weights, false, 10, true))
        console.log('biases:', util.inspect(this.biases, false, 10, true))
    }

    /**
     * Устанавливает параметры сети
     * @param options
     * @returns {*}
     */
    setOptions(options) {
        const self = this, // текущий контекст
              params = Object.keys(DEFAULT_OPTIONS); // список параметров сети

        // если параметры не переданы,
        // то оставим параметры по умолчанию
        if (!options) {
            this.options = DEFAULT_OPTIONS;
            return
        }

        this.options = {};

        // пройдемся по параметрам
        params.forEach(function (key) {
            // если такой параметр сети не существует
            if (typeof options[key] === 'undefined') {
                // установим параметр по умолчанию
                self.options[key] = DEFAULT_OPTIONS[key];
            }
            else { // иначе установим переданный
                self.options[key] = options[key];
            }
        });

        return this;
    }

    /**
     * Возвращает нейронную сеть в виде JSON-строки
     * для дальнейшего использования
     * @return {String}
     */
    toJSON() {
        return JSON.stringify({
            options : this.options,
            weights : this.weights,
            biases  : this.biases
        });
    }

    /**
     * Возвращает новый экземпляр Perceptron, загруженный из JSON-строки.
     * @param  {String} str
     * @return {Object} Perceptron
     */
    fromJSON(str) {

        let parsed; // данные из файла
        try {
            parsed = JSON.parse(str)
        } catch (err) {
            throw new Error('Perceptron.fromJSON: `str` не является допустимой строкой JSON.')
        }

        this.options = parsed.options;
        this.weights = parsed.weights;
        this.biases  = parsed.biases;

        return this;
    }

    /**
     * Вычисление сигмоидальной униполярной (значение принадлежит [0, 1])
     * функции активации
     * @return {number}
     */
    static activate(x) {
        return 1 / (1 + Math.exp( -1 * x ))
    }

}

